//
//  BugSuggestionViewController.swift
//  LeicaSurvey
//
//  Created by  on 12/19/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit
import Alamofire
import AudioToolbox
//https://docs.google.com/spreadsheets/d/1RN_bMO6RD-zL8_wCwEapin4nCqJkllcT8g6BQTBqALs/edit?usp=sharing
class BugSuggestionViewController: UIViewController {
    @IBOutlet weak var tf_Emailsubject: UITextField!
    @IBOutlet weak var tv_emailBody: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        //tf_Emailsubject.text = "marketing-medical@leica-microsystems.com"
        //tf_Emailsubject.text = "zohaibmuhammadf26@gmail.com"
        //tf_Emailsubject.isUserInteractionEnabled = false
    }
    @IBAction func cancelBtn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func sendBtn(_ sender: UIButton) {
        if !(tf_Emailsubject.text?.isEmpty ?? true) || !(tv_emailBody.text.isEmpty){
            self.SubmitBugsAndSuggestion(email_: tf_Emailsubject.text ?? "", emailbody: tv_emailBody.text ?? "")
        }
        else {
            self.alert(title: "Validation Failed", msg: "subject and detail can not be empty.")
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
    }
    func SubmitBugsAndSuggestion(email_:String,emailbody:String){
        let params = [
            "email":email_,
            "body":emailbody
        ]
        startAnimating()
        Alamofire.request(Constants.URL.raiseBugAndSuggestion, method: .post, parameters: params, headers: nil).responseJSON { (response) in
            self.stopAnimating()
            switch response.result {
            case .success(let json):
                print(json)
                let json = json as! Dictionary<String,Any>
                guard let code = json["code"] as? Int else {
                    fatalError("failed to get the code")
                }
                if code == 200 {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.alert(title: "Syntax Error", msg: "\(code)")
                }
                break
            case .failure(let error):
                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self.alert(title: "No Internet!", msg: "Please try again")
                    print(err)
                } else {
                    self.alert(title: "Failed", msg: "Server communication failed, please try again.")
                }
                
                break
            }
        }
    }
}
