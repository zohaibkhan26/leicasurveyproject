//
//  TipsTricksViewController.swift
//  LeicaSurvey
//
//  Created by  on 11/30/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit
import Alamofire
class TipsTricksViewController: UIViewController {
    var containerView:UIView!
    var heightOuterViewofContainer:NSLayoutConstraint!
    var parentClass:MainContainerViewController!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LoadWithAnimation()
    }
    @IBAction func StartsurveyBtn(_ sender: Any) {
        LoadContentUploadScreen()
    }
    
    @IBAction func TipstricksBtn(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TipsAndTrickViewViewController") as! TipsAndTrickViewViewController
        self.present(vc, animated: true, completion: nil)
    }
    func LoadContentUploadScreen() {
        CreateSurvey()
    }
    func CreateSurvey(){
        startAnimating()
        Alamofire.request(Constants.URL.createSurvey, method: .post, parameters: nil, headers: nil).responseJSON { (response) in
            self.stopAnimating()
            switch response.result {
            case .success(let json):
                print(json)
                let json = json as! Dictionary<String,Any>
                guard let code = json["code"] as? Int else {
                    fatalError("failed to get the code")
                }
                if code == 200 {
                    UserDefaults.standard.setValue(json["result"] as! Int, forKey: "surveyID")
                    self.loadUploadContentScreen()
                } else {
                    self.alert(title: "Syntax Error", msg: "\(code)")
                }
                break
            case .failure(let error):
                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self.alert(title: "No Internet!", msg: "Please try again")
                    print(err)
                } else {
                    self.alert(title: "Failed", msg: "Server communication failed, please try again.")
                }
                
                break
            }
        }
    }
    func loadUploadContentScreen(){
        parentClass.RemoveVC()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ContentUploadViewController") as! ContentUploadViewController
        parentClass.addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        //heightOuterViewofContainer.constant = 850
        viewController.heightOuterViewofContainer = self.heightOuterViewofContainer
        viewController.containerView = containerView
        viewController.parentClass = parentClass
        viewController.didMove(toParent: parentClass)
        parentClass.setupSteps(vc: viewController)
    }
}
