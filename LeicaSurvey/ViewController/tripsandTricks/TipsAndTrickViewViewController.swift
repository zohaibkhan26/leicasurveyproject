//
//  TipsAndTrickViewViewController.swift
//  LeicaSurvey
//
//  Created by Zohaib  Khan on 07/01/2020.
//  Copyright © 2020 norisofts. All rights reserved.
//

import UIKit

class TipsAndTrickViewViewController: UIViewController {
    @IBOutlet weak var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let url = Bundle.main.url(forResource: "tipsAndTricks", withExtension: "pdf")
        let request = NSURLRequest(url: url!)
        webview.loadRequest(request as URLRequest)
        
        webview.scrollView.showsHorizontalScrollIndicator = false
        webview.scrollView.showsVerticalScrollIndicator = false
    }
    @IBAction func backBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
