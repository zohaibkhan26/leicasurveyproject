//
//  CustomerSignUpViewController.swift
//  LeicaSurvey
//
//  Created by  on 12/2/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit
import AudioToolbox
import Alamofire

class CustomerSignUpViewController: UIViewController , YPSignatureDelegate{
    //tfs
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var tfname: UITextField!
    @IBOutlet weak var tfTitle: UITextField!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfHospitalName: UITextField!
    @IBOutlet weak var tfCity: UITextField!
    @IBOutlet weak var tfCountry: UITextField!
    @IBOutlet weak var tfDptEmail: UITextField!
    @IBOutlet weak var tfComments: UITextField!
    
    @IBOutlet weak var readmoreBtn: UIButton!
    var containerView:UIView!
    @IBOutlet weak var heightTxtView: NSLayoutConstraint!
    @IBOutlet weak var txtViewSummary: UITextView!
    @IBOutlet weak var viewHieht:NSLayoutConstraint!
    @IBOutlet weak var viewSignature: YPDrawSignatureView!
    var parentClass:MainContainerViewController!
    var imagesCount:Int!
    var videosCount:Int!
    var voiceRecordCount:Int!
    var isexpanded = false
    var isSigned = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LoadWithAnimation()
        viewSignature.delegate = self
        viewSignature.strokeColor = .blue
        txtViewSummary.text = "\(imagesCount ?? 0)- Images \n\(videosCount ?? 0)- Videos \n\(voiceRecordCount ?? 0)- Voice Records"
      //  tfEmail.text = ""
        
        tfname.setLeftPaddingPoints(4)
        tfname.setRightPaddingPoints(4)
        
        tfTitle.setLeftPaddingPoints(4)
        tfTitle.setRightPaddingPoints(4)
        
        tfEmail.setLeftPaddingPoints(4)
        tfEmail.setRightPaddingPoints(4)
        
        tfHospitalName.setLeftPaddingPoints(4)
        tfHospitalName.setRightPaddingPoints(4)
        
        tfCity.setLeftPaddingPoints(4)
        tfCity.setRightPaddingPoints(4)
        
        tfCountry.setLeftPaddingPoints(4)
        tfCountry.setRightPaddingPoints(4)
        
        tfDptEmail.setLeftPaddingPoints(4)
        tfDptEmail.setRightPaddingPoints(4)
        
        tfComments.setLeftPaddingPoints(4)
        tfComments.setRightPaddingPoints(4)
    }
    @IBAction func readMoreBtn(_ sender: UIButton) {
        if !isexpanded{
            isexpanded = true
            readmoreBtn.setTitle("Read Less", for: .normal)
            heightTxtView.constant = 388
            viewHieht.constant = 1367
        } else {
            isexpanded = false
            readmoreBtn.setTitle("Read More", for: .normal)
            heightTxtView.constant = 30
            viewHieht.constant = 988
        }
        
        
    }
    @IBAction func signDoc(_ sender: UIButton) {
        validation()
    }
    @IBAction func clearSigns(_ sender: UIButton) {
        isSigned = false
        viewSignature.clear()
    }
    func LoadSaleRef() {
        parentClass.RemoveVC()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "SaleRefeInformationViewController") as! SaleRefeInformationViewController
        parentClass.addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
       // heightOuterViewofContainer.constant = 400
        //viewController.heightOuterViewofContainer = self.heightOuterViewofContainer
        viewController.containerView = containerView
        viewController.parentClass = parentClass
        viewController.didMove(toParent: parentClass)
        parentClass.setupSteps(vc: viewController)
    }
    // MARK:- Signs delegate
    func didStart(_ view: YPDrawSignatureView) {
       scrollview.isScrollEnabled = false
    }
    func didFinish(_ view: YPDrawSignatureView) {
        isSigned = true
      scrollview.isScrollEnabled = true
    }
    
    
    func validation(){
        if tfname.text?.isEmpty ?? false || tfTitle.text?.isEmpty ?? false || tfEmail.text?.isEmpty ?? false || tfHospitalName.text?.isEmpty ?? false || tfCity.text?.isEmpty ?? false || tfCountry.text?.isEmpty ?? false{
            Alert(title: "Validation Failed!", msg: "Please fill all the mandatory fields.")
        }
        else if !isSigned {
            Alert(title: "Sign!", msg: "Please sign to proceed.")
        }
        else {
            if !self.isValidEmail(tfEmail.text!) {
               Alert(title: "Invalid Email", msg: "Please enter valid email.")
            } else {
                let base64Image = self.convertImageToBase64String(image: viewSignature.getSignature()!)
                self.submitStepTwo(sign:base64Image,name_to: tfname.text ?? "", title_to: tfTitle.text ?? "", email_to: tfEmail.text ?? "", hospital_to: tfHospitalName.text ?? "", city_to: tfCity.text ?? "", country_to: tfCountry.text ?? "", com_dep_email_to: tfDptEmail.text ?? "", comments_to: tfComments.text ?? "")
            }
            
        }
    }
    public func  convertImageToBase64String(image : UIImage ) -> String
    {
        let strBase64 =  image.pngData()?.base64EncodedString()
        return strBase64!
    }
    func Alert(title:String,msg:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .destructive) { (action) in
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
        //vibrate device
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    func submitStepTwo(sign:String,name_to:String,title_to:String,email_to:String,hospital_to:String,city_to:String,country_to:String,com_dep_email_to:String,comments_to:String){
        self.startAnimating()
        let params = [
            "name_to":name_to,
            "title_to": title_to,
            "email_to":email_to,
            "hospital_to":hospital_to,
            "city_to":city_to,
            "country_to":country_to,
            "com_dep_email_to":com_dep_email_to,
            "comments_to":comments_to,
            "image_name":sign,
            "survey_id": UserDefaults.standard.value(forKey: "surveyID") as! Int
            ] as [String : Any]
        Alamofire.request(Constants.URL.addStepTwo, method: .post, parameters: params, headers: nil).responseJSON { (response) in
            self.stopAnimating()
            switch response.result {
            case .success(let json):
                print(json)
                let json = json as! Dictionary<String,Any>
                guard let code = json["code"] as? Int else {
                    fatalError("failed to get the code")
                }
                if code == 200 {
                    self.LoadSaleRef()
                } else {
                    self.alert(title: "Syntax Error", msg: "\(code)")
                }
                break
            case .failure(let error):
                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self.alert(title: "No Internet!", msg: "Please try again")
                    print(err)
                } else {
                    self.alert(title: "Failed", msg: "Server communication failed, please try again.")
                }
                break
            }
        }
    }
}

extension CustomerSignUpViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfEmail {
//            if string == "@" {
//                let txt = textField.text ?? ""
//                textField.text = txt + "@leica-microsystems.com"
//                return false
//            }
        }
        return true
    }
    
}
