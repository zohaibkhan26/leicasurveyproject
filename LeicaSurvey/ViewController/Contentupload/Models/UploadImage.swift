//
//  UploadImage.swift
//  LeicaSurvey
//
//  Created by  on 12/19/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import Foundation
struct UploadImage : Codable {
    let code : Int?
    let image : String?
    let message : String?
    let result : String?
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case image = "image"
        case message = "message"
        case result = "result"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        result = try values.decodeIfPresent(String.self, forKey: .result)
    }
    
}
