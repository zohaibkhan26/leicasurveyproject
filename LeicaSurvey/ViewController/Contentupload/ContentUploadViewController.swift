//
//  ContentuploadViewController.swift
//  LeicaSurvey
//
//  Created by  on 11/30/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit
import MediaPlayer
import Kingfisher
import DropDown
import AVFoundation
import OpalImagePicker
import Photos
class ContentUploadViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate, MPMediaPickerControllerDelegate {
    @IBOutlet weak var viewDropDown: UIView!
    @IBOutlet weak var lblClinicalSegment: UILabel!
    @IBOutlet weak var picCollectionView: UICollectionView!
    @IBOutlet weak var videoCollectionView: UICollectionView!
    @IBOutlet weak var soundCollectionView: UICollectionView!
    @IBOutlet weak var tv_customerInterviewQuotes: UITextView!
    @IBOutlet weak var tv_ContextDescription: UITextView!
    var containerView:UIView!
    var heightOuterViewofContainer:NSLayoutConstraint!
    var parentClass:MainContainerViewController!
    //data vars
    var imagePickerPic = UIImagePickerController()
    var ImageSelectedArray = [String]()
    var videoSelectedArraythumb = [String]()
    var recordings = [URL]()
    var isImageSelected = ("pic","video","sound")
    var mediaType = ""
    var videoURL : NSURL?
    var uploadImage:UploadImage!
    let dropDown = DropDown()
    override func viewDidLoad() {
        super.viewDidLoad()
        picCollectionView.delegate = self
        videoCollectionView.delegate = self
        soundCollectionView.delegate = self
        picCollectionView.dataSource = self
        videoCollectionView.dataSource = self
        soundCollectionView.dataSource = self
        self.LoadWithAnimation()
    }
    @IBAction func clinicalSegMentDropDown(_ sender: UIButton) {
        
        // The view to which the drop down will appear on
        dropDown.anchorView = viewDropDown // UIView or UIBarButtonItem
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["NEUROSURGERY", "OPHTHALMOLOGY", "ENT","PRS","DENTAL","OTHER"]
        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
          print("Selected item: \(item) at index: \(index)")
            self.lblClinicalSegment.text = item
            self.dropDown.hide()
        }
        // Will set a custom width instead of the anchor view width
        //dropDown.width = 200
        dropDown.show()
    }
    @IBAction func videosBtn(_ sender: UIButton) {
       mediaType = isImageSelected.1
       VideoPickUp()
    }
    @IBAction func uploadBtn(_ sender: UIButton) {
        mediaType = isImageSelected.0
         ImagePickUp()
    }
    @IBAction func soubdBtn(_ sender: UIButton) {
        mediaType = isImageSelected.2
       //  soundPickup()
        sounddAlert()
    }
    @IBAction func nextBtn(_ sender: UIButton) {
        if lblClinicalSegment.text! != "Please Select" {
          self.submitStepOne(clinicalSegment: lblClinicalSegment.text!,customer_interview: tv_customerInterviewQuotes.text ?? "", context_desc: tv_ContextDescription.text ?? "")
        } else {
            self.alert(title: "Validation Failed!", msg: "Please select Clinical Segment.")
        }
        
    }
    func LoadCustomerSignUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CustomerSignUpViewController") as! CustomerSignUpViewController
        viewController.imagesCount = ImageSelectedArray.count
        viewController.videosCount = videoSelectedArraythumb.count
        viewController.voiceRecordCount = recordings.count
        parentClass.RemoveVC()
        parentClass.addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        //heightOuterViewofContainer.constant = 1000
       // viewController.heightOuterViewofContainer = self.heightOuterViewofContainer
        viewController.containerView = containerView
        viewController.parentClass = parentClass
        viewController.didMove(toParent: parentClass)
        parentClass.setupSteps(vc: viewController)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == picCollectionView {
            return ImageSelectedArray.count
        }
        else if collectionView == videoCollectionView{
            return videoSelectedArraythumb.count
        }
        else {
            return recordings.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let picCell = picCollectionView.dequeueReusableCell(withReuseIdentifier: "PicCollectionViewCell", for: indexPath) as! PicCollectionViewCell
        let videoCell = videoCollectionView.dequeueReusableCell(withReuseIdentifier: "VideosCollectionViewCell", for: indexPath) as! VideosCollectionViewCell
        let soundCell = soundCollectionView.dequeueReusableCell(withReuseIdentifier: "SoundCollectionViewCell", for: indexPath) as! SoundCollectionViewCell
        if collectionView == picCollectionView {
            if ImageSelectedArray.count>0{
                picCell.imgselected.kf.setImage(with: URL(string: self.ImageSelectedArray[indexPath.row]))
            }
            return picCell
        }
        else if collectionView == videoCollectionView {
            if videoSelectedArraythumb.count>0{
                let url = URL(string:  videoSelectedArraythumb[indexPath.row])
                if let thumbnailImage = getThumbnailImage(forUrl: url!) {
                    videoCell.videoSelected.image = thumbnailImage
                }
                
            }
            return videoCell
        }
        else {
            return soundCell
        }
    }
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)

        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
        }

        return nil
    }
    //MARK:- patient image delegate
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if mediaType == isImageSelected.0{
            let images = info[.originalImage]
            uploadImageFile(image:images as! UIImage, type: 1)
        } else if mediaType == isImageSelected.1 {
            videoURL = info[UIImagePickerController.InfoKey.mediaURL]as? NSURL
            uploadVideoFile(videoUrl: videoURL! as URL, type: 2)
            print(videoURL!)
            self.videoCollectionView.reloadData()
           
        }
        dismiss(animated: true)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true) {
            print("image cancel picker called")
        }
    }
    func ImagePickUp() {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallery()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func VideoPickUp() {
        let alert = UIAlertController(title: "Choose Video", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.cameraVideo()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.galleryVideo()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func openGallery()
    {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
            let imagePicker = OpalImagePickerController()
            imagePicker.imagePickerDelegate = self
            present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    func galleryVideo()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.savedPhotosAlbum) {
            let videoPicker = UIImagePickerController()
            videoPicker.delegate = self
            videoPicker.sourceType = .savedPhotosAlbum
            videoPicker.mediaTypes = [kUTTypeMovie as String]
            self.present(videoPicker, animated: true, completion: nil)
        }
    }
    
    func cameraVideo()
    {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let controller = UIImagePickerController()
            controller.sourceType = .camera
            controller.mediaTypes = [kUTTypeMovie as String]
            controller.delegate = self
            present(controller, animated: true, completion: nil)
        }
        else {
            print("Camera is not available")
        }
    }
    
    func sounddAlert() {
        let alert = UIAlertController(title: "Choose Audio", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Record", style: .default, handler: { _ in
            self.alert(title: "Coming Soon", msg: "Once you (Clemen) approve this feature, will add else will remove.")
        }))
        
        alert.addAction(UIAlertAction(title: "Phone", style: .default, handler: { _ in
            self.soundPickup()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func soundPickup(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypeAudio),String(kUTTypeMP3),String(kUTTypeMIDIAudio),"public.audio","public.audiovisual-content","public.audiovisual-content","public.data"], in: .import)
        importMenu.delegate = self
         importMenu.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
}
//MARK:- Audios Delegates
extension ContentUploadViewController: UIDocumentMenuDelegate,UIDocumentPickerDelegate, UIPopoverPresentationControllerDelegate {
    func prepareForPopoverPresentation(_ popoverPresentationController: UIPopoverPresentationController) {
        
    }
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else {
            return
        }
        print("import result : \(myURL)")
        uploadAudioFile(audioUrl: myURL, type: 3)
    }
    
    public func documentMenu(_ documentMenu:UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
//MARK:- images delegates multiple
extension ContentUploadViewController:OpalImagePickerControllerDelegate{
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingAssets assets: [PHAsset]){
        print(assets)
        let images =  getAssetThumbnail(assets: assets)
        for i in images {
            uploadImageFile(image: i, type: 1)
        }
        presentedViewController?.dismiss(animated: true, completion: nil)
    }
    func imagePickerDidCancel(_ picker: OpalImagePickerController){
        
    }
    func imagePicker(_ picker: OpalImagePickerController, didFinishPickingExternalURLs urls: [URL]){
        print(urls)
    }
    
    //MARK: Convert array of PHAsset to UIImages
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var image = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                image = result!
                arrayOfImages.append(image)
            })
        }

        return arrayOfImages
    }
}
