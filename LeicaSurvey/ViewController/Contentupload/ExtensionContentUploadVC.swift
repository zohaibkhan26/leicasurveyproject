//
//  ExtensionContentUploadVC.swift
//  LeicaSurvey
//
//  Created by  on 12/18/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import Foundation
import AVFoundation
import Alamofire
import MBProgressHUD
extension ContentUploadViewController{
    
    func submitImage(base64Image:String, type: Int){
        print("***********************************************************")
        print(base64Image)
        print("###########################################################")
        self.startAnimating()
        let params = [
            "type":type,
            "survey_id": UserDefaults.standard.value(forKey: "surveyID") as! Int,
            "image_name": base64Image
            ] as [String : Any]
        Alamofire.request(Constants.URL.uploadMedia, method: .post, parameters: params, headers: nil).responseJSON { (response) in
            self.stopAnimating()
            switch response.result {
            case .success(let json):
                let json = json as! Dictionary<String,Any>
                guard let data = response.data else {
                    return
                }
                self.handleresponse(json: json, data:data,type: type)
                break
            case .failure(let error):
                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self.alert(title: "No Internet!", msg: "Please try again")
                    print(err)
                } else {
                    self.alert(title: "Failed", msg: "Server communication failed, please try again.")
                }
                
                break
            }
        }
    }
    
    func handleresponse(json: [String : Any], data:Data,type: Int){
        guard let code = json["code"] as? Int else {
            fatalError("failed to get the code")
        }
        if code == 200 {
            do {
                let decoder = JSONDecoder()
                self.uploadImage = try decoder.decode(UploadImage.self, from: data)
            } catch  {
                print(error)
            }
            if type == 1 {
                self.ImageSelectedArray.append(self.uploadImage.image ?? "")
                self.picCollectionView.reloadData()
            } else if type == 2 {
                self.videoSelectedArraythumb.append(self.uploadImage.image ?? "")
                self.videoCollectionView.reloadData()
            } else {
                self.recordings.append((URL(string: self.uploadImage.image ?? "") ?? nil)!)
                self.soundCollectionView.reloadData()
            }
            
        } else {
            self.alert(title: "Syntax Error", msg: "\(code)")
        }
    }

    func submitStepOne(clinicalSegment:String,customer_interview:String, context_desc:String){
        startAnimating()
        let params = [
            "clinicalSegment":clinicalSegment,
            "customer_interview":customer_interview,
            "context_desc": context_desc,
            "survey_id": UserDefaults.standard.value(forKey: "surveyID") as! Int
            ] as [String : Any]
        Alamofire.request(Constants.URL.addStepOne, method: .post, parameters: params, headers: nil).responseJSON { (response) in
            self.stopAnimating()
            switch response.result {
            case .success(let json):
                print(json)
                let json = json as! Dictionary<String,Any>
                guard let code = json["code"] as? Int else {
                    fatalError("failed to get the code")
                }
                if code == 200 {
                    self.LoadCustomerSignUp()
                } else {
                    self.alert(title: "Syntax Error", msg: "\(code)")
                }
                break
            case .failure(let error):
                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self.alert(title: "No Internet!", msg: "Please try again")
                    print(err)
                } else {
                    self.alert(title: "Failed", msg: "Server communication failed, please try again.")
                }
                break
            }
        }
    }
 
    func uploadImageFile(image:UIImage, type:Int){
          // startAnimating()
            let surveyID = UserDefaults.standard.value(forKey: "surveyID") as! Int
            //let image = UIImage.init(named: "testt")
            let imgData = image.jpegData(compressionQuality: 0.2)!
            let url = Constants.URL.upload_media_file
            let headers = ["Content-Type": "multipart/form-data"]
            print(url)
            let parameters: [String : String] = ["type": "\(type)",
                "survey_id": "\(surveyID)"
                ] as [String:String]
        
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            hud.mode = MBProgressHUDMode.annularDeterminate
            hud.label.text = "Uploading…"
        
            Alamofire.upload(multipartFormData:{ multipartFormData in
                multipartFormData.append(imgData, withName: "image_name", fileName: "file.jpeg", mimeType: "image/jpeg")
                for (key, value) in parameters
                {
                    multipartFormData.append((value).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }, to: url, method: .post, headers: headers)
            { (result) in
                  
                switch result {
                case .success(let upload,_,_ ):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                     
                        hud.progress = Float(progress.fractionCompleted)
                    
                    })
                    upload.responseJSON
                        { response in
                            
                            hud.hide(animated: true)
                            
                            let json = response.result.value as! Dictionary<String,Any>
                            print(json)
                            guard let data = response.data else {
                                return
                            }
                            self.handleresponse(json: json, data:data,type: type)
                            
                    }
                case .failure( _):
                     self.stopAnimating()
                     hud.hide(animated: true)
                    break
                }
            }
        
    }
    
    func uploadVideoFile(videoUrl:URL, type:Int){
        startAnimating()
            let surveyID = UserDefaults.standard.value(forKey: "surveyID") as! Int
            //let image = UIImage.init(named: "testt")
            let url = Constants.URL.upload_media_file
            let headers = ["Content-Type": "multipart/form-data"]
            print(url)
            let parameters: [String : String] = ["type": "\(type)",
                "survey_id": "\(surveyID)"
                ] as [String:String]
            
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                   hud.mode = MBProgressHUDMode.annularDeterminate
                   hud.label.text = "Uploading…"
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(videoUrl, withName: "image_name", fileName: "video.mp4", mimeType: "video/mp4")
            for (key, value) in parameters
            {
                multipartFormData.append((value).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
        }, to: url, method: .post, headers: headers)
        { (result) in
               self.stopAnimating()
                switch result {
                case .success(let upload,_,_ ):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                         hud.progress = Float(progress.fractionCompleted)
                    })
                    upload.responseJSON
                        { response in
                            hud.hide(animated: true)
                            
                            let json = response.result.value as! Dictionary<String,Any>
                            print(json)
                            guard let data = response.data else {
                                return
                            }
                            self.handleresponse(json: json, data:data,type: type)
                            
                    }
                case .failure( _):
                    hud.hide(animated: true)
                    break
                }
            }
        
    }
    
    func uploadAudioFile(audioUrl:URL, type:Int){
        startAnimating()
            let surveyID = UserDefaults.standard.value(forKey: "surveyID") as! Int
            //let image = UIImage.init(named: "testt")
            let url = Constants.URL.upload_media_file
            let headers = ["Content-Type": "multipart/form-data"]
            print(url)
            let parameters: [String : String] = ["type": "\(type)",
                "survey_id": "\(surveyID)"
                ] as [String:String]
            
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
                   hud.mode = MBProgressHUDMode.annularDeterminate
                   hud.label.text = "Uploading…"
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(audioUrl, withName: "image_name", fileName: "audio.mp3", mimeType: "audio/mp3")
            for (key, value) in parameters
            {
                multipartFormData.append((value).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
        }, to: url, method: .post, headers: headers)
        { (result) in
               self.stopAnimating()
                switch result {
                 
                case .success(let upload,_,_ ):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                         hud.progress = Float(progress.fractionCompleted)
                    })
                    upload.responseJSON
                        { response in
                            hud.hide(animated: true)
                            let json = response.result.value as! Dictionary<String,Any>
                            print(json)
                            guard let data = response.data else {
                                return
                            }
                            self.handleresponse(json: json, data:data,type: type)
                            
                    }
                case .failure( _):
                    hud.hide(animated: true)
                    break
                }
            }
        
    }

}
