//
//  SaleRefeInformationViewController.swift
//  LeicaSurvey
//
//  Created by  on 12/2/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit
import AudioToolbox
import Alamofire
class SaleRefeInformationViewController: UIViewController {
    var containerView:UIView!
  //  var heightOuterViewofContainer:NSLayoutConstraint!
    var parentClass:MainContainerViewController!
    var isSwitchOneChecked = true
    var isSwitchTwoChecked = true
    var isSwitchThreeChecked = true
    @IBOutlet weak var tfemailProf: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LoadWithAnimation()
        tfemailProf.setLeftPaddingPoints(4)
        tfemailProf.setRightPaddingPoints(4)
    }
    @IBAction func nextBtn(_ sender: UIButton) {
        if isSwitchOneChecked && isSwitchTwoChecked && isSwitchThreeChecked && !(tfemailProf.text?.isEmpty ?? false) && self.isValidEmail(tfemailProf.text ?? ""){
            LoadInformationDispatch()
        }
        else if (tfemailProf.text?.isEmpty ?? true) || !self.isValidEmail(tfemailProf.text ?? ""){
            let alert = UIAlertController(title: "Validation Failed!", message: "Please Enter the email.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .destructive) { (action) in
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            //vibrate device
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
        else {
            let alert = UIAlertController(title: "Boxes Tick!", message: "Please tick all the boxes to proceed.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .destructive) { (action) in
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
            //vibrate device
            AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
        }
    }
    @IBAction func firstSwitch(_ sender: UISwitch) {
        isSwitchOneChecked = sender.isOn
    }
    @IBAction func secondSwitch(_ sender: UISwitch) {
        isSwitchTwoChecked = sender.isOn
    }
    @IBAction func thirdSwitch(_ sender: UISwitch) {
        isSwitchThreeChecked = sender.isOn
    }
    func LoadInformationDispatch() {
        parentClass.RemoveVC()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "InformationDispatchViewController") as! InformationDispatchViewController
        parentClass.addChild(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.containerView = containerView
        viewController.parentClass = parentClass
        viewController.profEmail = tfemailProf.text ?? ""
        viewController.didMove(toParent: parentClass)
        parentClass.setupSteps(vc: viewController)
    }
}
extension SaleRefeInformationViewController:UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == tfemailProf {
            if string == "@" {
                let txt = textField.text ?? ""
                textField.text = txt + "@leica-microsystems.com"
                return false
            }
        }
        return true
    }
}
