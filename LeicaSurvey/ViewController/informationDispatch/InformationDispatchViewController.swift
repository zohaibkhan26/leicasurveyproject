//
//  InformationDispatchViewController.swift
//  LeicaSurvey
//
//  Created by  on 12/2/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit
import Alamofire

class InformationDispatchViewController: UIViewController {
    var containerView:UIView!
    var heightOuterViewofContainer:NSLayoutConstraint!
    var parentClass:MainContainerViewController!
    var profEmail:String = ""
    @IBOutlet weak var txtView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LoadWithAnimation()
        txtView.text = "All these information are going to be sent to :" +
        "\n\u{2022} your marketing team for processing" +
        "\n\u{2022} your customer as summary" +
        "\n\u{2022} you, for your own records and as reminder to send clinical images and videos"
    }
    @IBAction func infoDispatch(_ sender: UIButton) {
        submitStepThreeAndFour()
    }
    func submitStepThreeAndFour(){
        self.startAnimating()
        let params = [
            "prof_email":profEmail,
            "survey_id": UserDefaults.standard.value(forKey: "surveyID") as! Int
            ] as [String : Any]
        Alamofire.request(Constants.URL.addStepThree, method: .post, parameters: params, headers: nil).responseJSON { (response) in
            self.stopAnimating()
            switch response.result {
            case .success(let json):
                print(json)
                let json = json as! Dictionary<String,Any>
                guard let code = json["code"] as? Int else {
                    fatalError("failed to get the code")
                }
                if code == 200 {
                    self.alert()
                } else {
                    self.alert(title: "Syntax Error", msg: "\(code)")
                }
                break
            case .failure(let error):
                print(error)
                if let err = error as? URLError, err.code == .notConnectedToInternet {
                    self.alert(title: "No Internet!", msg: "Please try again")
                    print(err)
                } else {
                    self.alert(title: "Failed", msg: "Server communication failed, please try again.")
                }
                break
            }
        }
    }
    func alert(){
        let alert = UIAlertController(title: "Submitted!", message: "\n\n\n", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Done", style: .destructive, handler: { (action) in
            self.parentClass.navigationController?.popViewController(animated: true)
        }))
        let image = UIImageView(image: #imageLiteral(resourceName: "doneicongreen"))
        alert.view.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerY, relatedBy: .equal, toItem: alert.view, attribute: .centerY, multiplier: 1, constant: 0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 64.0))
        alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 64.0))
        self.present(alert, animated: true, completion: nil)
    }
}
