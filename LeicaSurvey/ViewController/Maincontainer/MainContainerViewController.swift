//
//  MainContainerViewController.swift
//  LeicaSurvey
//
//  Created by  on 11/30/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit

class MainContainerViewController: UIViewController {
    //@IBOutlet weak var heightOuterViewofContainer: NSLayoutConstraint!
    @IBOutlet weak var MainContainerView: UIView!
    @IBOutlet weak var lblCaptureTestimonial: UILabel!
    @IBOutlet weak var ViewOne: UIView!
    @IBOutlet weak var lblOne: UILabel!
    @IBOutlet weak var ViewTwo: UIView!
    @IBOutlet weak var lblTwo: UILabel!
    @IBOutlet weak var ViewThree: UIView!
    @IBOutlet weak var lblThree: UILabel!
    @IBOutlet weak var ViewFour: UIView!
    @IBOutlet weak var lblFour: UILabel!
    @IBOutlet weak var lblStepOne: UILabel!
    @IBOutlet weak var lblStepTwo: UILabel!
    @IBOutlet weak var lblStepThree: UILabel!
    @IBOutlet weak var lblStepFour: UILabel!
    //steps views
    override func viewDidLoad() {
        super.viewDidLoad()
        LoadStartSurveyScreen()
    }
    func LoadStartSurveyScreen() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "TipsTricksViewController") as! TipsTricksViewController
        self.addChild(viewController)
        MainContainerView.addSubview(viewController.view)
        viewController.view.frame = MainContainerView.bounds
       // heightOuterViewofContainer.constant = 400
       // viewController.heightOuterViewofContainer = self.heightOuterViewofContainer
        viewController.containerView = self.MainContainerView
        viewController.parentClass = self
        viewController.didMove(toParent: self)
        setupSteps(vc: viewController)
    }
    func RemoveVC(){
        for v in self.MainContainerView.subviews{
            v.removeFromSuperview()
        }
    }
    func setupSteps(vc:UIViewController){
        switch vc {
        case is TipsTricksViewController:
            ViewOne.backgroundColor = .gray
            ViewTwo.backgroundColor = .gray
            ViewThree.backgroundColor = .gray
            ViewFour.backgroundColor = .gray
            break
        case is ContentUploadViewController:
            ViewOne.backgroundColor = .red
            ViewTwo.backgroundColor = .gray
            ViewThree.backgroundColor = .gray
            ViewFour.backgroundColor = .gray
            break
        case is CustomerSignUpViewController:
            ViewOne.backgroundColor = .gray
            ViewTwo.backgroundColor = .red
            ViewThree.backgroundColor = .gray
            ViewFour.backgroundColor = .gray
            break
        case is SaleRefeInformationViewController:
            ViewOne.backgroundColor = .gray
            ViewTwo.backgroundColor = .gray
            ViewThree.backgroundColor = .red
            ViewFour.backgroundColor = .gray
            break
        case is InformationDispatchViewController:
            ViewOne.backgroundColor = .gray
            ViewTwo.backgroundColor = .gray
            ViewThree.backgroundColor = .gray
            ViewFour.backgroundColor = .red
            break
        default:
            break
        }
    }
    
}
