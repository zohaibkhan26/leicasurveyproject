//
//  ViewController.swift
//  LeicaSurvey
//
//  Created by Muhammad Sajjad  on 30/11/2019.
//  Copyright © 2019 norisofts. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func CapturetestimonialBtn(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        // Instantiate View Controller
        let viewController = storyboard.instantiateViewController(withIdentifier: "MainContainerViewController") as! MainContainerViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    @IBAction func StartdemosurveyBtn(_ sender: Any) {
        uploadtestVideo()
    }
    @IBAction func RaisebugBtn(_ sender: Any) {
//       self.definesPresentationContext = true
//        self.modalTransitionStyle = .crossDissolve
        let yourVC = self.storyboard?.instantiateViewController(withIdentifier: "BugSuggestionViewController") as! BugSuggestionViewController
        let navController = UINavigationController(rootViewController: yourVC)
        navController.modalPresentationStyle = .overCurrentContext
        navController.modalTransitionStyle = .flipHorizontal
        self.present(navController, animated: true, completion: nil)
    }

    func uploadtestVideo(){
        let image = UIImage.init(named: "testt")
        let imgData = image!.jpegData(compressionQuality: 0.2)!
        let url = Constants.URL.upload_media_file
        let headers = ["Content-Type": "multipart/form-data"]
        print(url)
        let parameters: [String : String] = ["type": "\(1)",
            "survey_id": "\(202)"
            ] as [String:String]
        
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(imgData, withName: "image_name", fileName: "file.jpeg", mimeType: "image/jpeg")
            for (key, value) in parameters
            {
                multipartFormData.append((value).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
            }
        }, to: url, method: .post, headers: headers)
        { (result) in
            switch result {
            case .success(let upload,_,_ ):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON
                    { response in
                        let json = response.result.value
                        print(json)
                }
            case .failure( _):
                break
            }
        }
    }
}

