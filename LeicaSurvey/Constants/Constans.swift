//
//  Constans.swift
//  LeicaSurvey
//
//  Created by  on 12/18/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import Foundation
class Constants {
    static var baseUrl: String {
        return "http://lmsstb.com/api/"
    }
    static var baseUrlMedia: String {
        return "http://lmsstb.com/uploads/image/"
    }
    struct URL {
        static let createSurvey = Constants.baseUrl + "create_survey"
        static let uploadMedia = Constants.baseUrl + "upload_media_old"
        static let addStepOne = Constants.baseUrl + "add_step_one"
        static let addStepTwo = Constants.baseUrl + "add_step_two"
        static let addStepThree = Constants.baseUrl + "add_step_three"
        static let raiseBugAndSuggestion = Constants.baseUrl + "raise_bug_suggestions"
        static let upload_media_file = Constants.baseUrl + "upload_media"
    }
}
