//
//  ENUMS.swift
//  LeicaSurvey
//
//  Created by  on 12/18/19.
//  Copyright © 2019 norisofts. All rights reserved.
//

import Foundation
enum Codes:Int{
    case ok = 200
    case syntaxError = 500
    case pagenot = 404
}
